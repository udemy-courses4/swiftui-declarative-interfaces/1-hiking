//
//  ContentView.swift
//  Hiking
//
//  Created by Sander de Groot on 14/08/2020.
//

import SwiftUI

struct ContentView: View {
    
    let hikes = Hike.all()
    
    var body: some View {
        
        NavigationView {
            List(self.hikes, id: \.name) { hike in
                NavigationLink(destination: HikeDetailsView(hike: hike)) {
                    HikeCell(hike: hike)
                }
            }
            .navigationBarTitle("Hikes")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct HikeCell: View {
    
    let hike: Hike
    
    var body: some View {
        HStack {
            Image(hike.picture)
                .resizable()
                .frame(width: 100, height: 100, alignment: .center)
                .cornerRadius(10)
            
            VStack(alignment: .leading) {
                Text(hike.name)
                Text(String(format: "%.1f miles", hike.miles))
            }
        }
    }
}

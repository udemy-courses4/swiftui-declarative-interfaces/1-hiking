//
//  HikeDetailsView.swift
//  Hiking
//
//  Created by Sander de Groot on 14/08/2020.
//

import SwiftUI

struct HikeDetailsView: View {
    
    let hike: Hike
    @State private var zoomed = false
    
    var body: some View {
        VStack {
            Image(hike.picture)
                .resizable()
                .aspectRatio(contentMode: self.zoomed ? .fill : .fit)
                .onTapGesture {
                    withAnimation {
                        self.zoomed.toggle()
                    }
                }
            
            Text(hike.name)
            Text(String(format: "%.1f miles", hike.miles))
        }
        .navigationBarTitle(Text(hike.name), displayMode: .inline)
    }
}

struct HikeDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        let hike = Hike.all().first!
        return HikeDetailsView(hike: hike)
    }
}

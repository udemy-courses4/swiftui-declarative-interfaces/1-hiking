//
//  Hike.swift
//  Hiking
//
//  Created by Sander de Groot on 14/08/2020.
//

import Foundation

struct Hike {
    
    let name: String
    let picture: String
    let miles: Double
}

extension Hike {
    
    static func all() -> [Hike] {
        return [
            Hike(name: "The Zion Narrows", picture: "tom", miles: 16.0),
            Hike(name: "Trolltunga", picture: "sal", miles: 14.3),
            Hike(name: "Angels Landing", picture: "tam", miles: 5.4)
        ]
    }
}
